function tinhPhanTramThue(tongThuNhap) {
  var phanTramThue = 0;
  if (tongThuNhap > 960000000) {
    phanTramThue = tongThuNhap * 0.35;
  } else if (tongThuNhap <= 960 && tongThuNhap > 624) {
    phanTramThue = tongThuNhap * 0.3;
  } else if (tongThuNhap <= 624 && tongThuNhap > 384) {
    phanTramThue = tongThuNhap * 0.25;
  } else if (tongThuNhap <= 384 && tongThuNhap > 210) {
    phanTramThue = tongThuNhap * 0.2;
  } else if (tongThuNhap <= 210 && tongThuNhap > 120) {
    phanTramThue = tongThuNhap * 0.15;
  } else if (tongThuNhap <= 120 && tongThuNhap > 60) {
    phanTramThue = tongThuNhap * 0.1;
  } else {
    phanTramThue = tongThuNhap * 0.05;
  }
  return phanTramThue;
}
function btnTinhThue() {
  var hoTen = document.getElementById("hoTen").value;
  var tongThuNhapNam = document.getElementById("tongThuNhapNam").value;
  var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value;
  var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;
  var thueThuNhapCaNhan = tinhPhanTramThue(thuNhapChiuThue);
  console.log("thueThuNhapCaNhan: ", thueThuNhapCaNhan);
  if (thueThuNhapCaNhan < 0) {
    document.getElementById(
      "ketQua"
    ).innerHTML = `${hoTen} không phải chịu thuế thu nhập cá nhân`;
  } else {
    document.getElementById(
      "ketQua"
    ).innerHTML = `Thuế thu nhập cá nhân mà ${hoTen} phải chịu là: ${thueThuNhapCaNhan.toLocaleString()} đồng `;
  }
}
